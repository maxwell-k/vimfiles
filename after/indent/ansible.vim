" after/indent/ansible.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
set autoindent& cindent& smartindent& indentexpr&
