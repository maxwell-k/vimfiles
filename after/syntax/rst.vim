" after/syntax/rst.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
syntax sync clear
syntax spell toplevel
