" after/ftplugin/python.vim
" Copyright 2022 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
"After /usr/share/vim/vim82/ftplugin/python.vim:42
set omnifunc=ale#completion#OmniFunc
