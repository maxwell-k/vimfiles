" syntax/frontmatter.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
" Use with a comment to set ft=markdown.gfm.frontmatter
" implement highlighting for https://dev.to/p/editor_guide
syntax region frontmatter
  \ start=/\%1l^---$/ end=/^---$/
  \ contains=@spell,frontmatterKeyword,mkdInlineURL
syntax keyword frontmatterKeyword
  \ title
  \ published
  \ description
  \ tags
  \ canonical_url
  \ cover_image
  \ series
  \ contained
highlight default link frontmatterKeyword Keyword
