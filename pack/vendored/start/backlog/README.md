# Backlog

This is a format for task lists:

<!-- embedme backlog.txt -->

```todo
2025-01-01 +project Immediate task
{{{
2025-01-01 +project Task
{{{
2025-01-01 +project-1 Later task
2025-01-01 +project-2 Later task @context-2
2025-01-01 +project-3 Later task @context-1

```

Features:

1. Sort the later tasks by project (default) or by context
1. Make an immediate task "later" and sort
1. Make a later task immediate and add any necessary marker

Inspired by:

- <http://todotxt.org/>
- <https://gitlab.com/dbeniamine/todo.txt-vim.git>

<!--
# pack/vendored/start/backlog/README.md
# Copyright Keith Maxwell
# SPDX-License-Identifier: CC0-1.0
-->
<!-- vim: set filetype=markdown.embedme.htmlCommentNoSpell : -->
