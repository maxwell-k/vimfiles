" pack/vendored/start/gopass/ftplugin/gopass.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
setlocal conceallevel=1
syntax enable
