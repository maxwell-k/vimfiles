" ftplugin/wide.vim
" Copyright 2021 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
" used via filetype.gitignored.vim as json.wide
let b:ale_json_jq_options = '--indent 4'
