" ftplugin/jsonnet.vim
" Copyright 2023 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
packadd jsonnet
let b:ale_fixers = ['jsonnetfmt']
