" ftplugin/go.vim
" Copyright 2021 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
let b:ale_fixers=['gofmt']
