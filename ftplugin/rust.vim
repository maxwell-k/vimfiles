" ftplugin/rust.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
let g:racer_experimental_completer = 1
let b:ale_fixers = ['rustfmt']
