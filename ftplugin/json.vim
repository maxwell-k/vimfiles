" ftplugin/json.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
let b:ale_fixers = get(b:, 'ale_fixers', ['jq'])
