" ftplugin/email.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
setlocal makeprg=email.sh\ %\ ~/.Downloads/%:t.html
