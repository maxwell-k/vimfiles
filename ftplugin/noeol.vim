" ftplugin/noeol.vim
" Copyright 2024 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
setlocal nofixendofline noeol
