" ftplugin/requirements.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
setlocal nowrap
packadd requirements.txt
