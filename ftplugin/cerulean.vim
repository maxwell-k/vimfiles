" ftplugin/cerulean.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
setlocal makeprg=cerulean.py\ --input-encoding=utf-8\ %\ %.html
