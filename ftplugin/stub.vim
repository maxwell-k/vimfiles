" ftplugin/stub.vim
" Copyright 2024 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
" Python filetype settings for .pyi files; use as python.stub
"
let b:ale_python_flake8_options = '--ignore=E301,E302,E704'
