" ftplugin/remark.vim
" Copyright 2021 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
" for use with https://github.com/gnab/remark/ https://remarkjs.com/
let b:ale_fixers = []
setlocal spellcapcheck=
