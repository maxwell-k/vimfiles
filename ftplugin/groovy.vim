" ftplugin/groovy.vim
" Copyright 2021 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
set shiftwidth=4
set softtabstop=4
