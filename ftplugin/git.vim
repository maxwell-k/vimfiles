" ftplugin/git.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
setlocal colorcolumn=72
setlocal nowrap
