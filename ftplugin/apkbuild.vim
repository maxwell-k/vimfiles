" ftplugin/apkbuild.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
let g:ale_sh_shellcheck_exclusions = 'SC2034,SC2154,SC2164'
let b:ale_fixers = ['remove_trailing_lines', 'trim_whitespace']
