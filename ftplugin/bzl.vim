"
" ftplugin/bzl.vim
" Copyright 2025 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
let b:ale_fixers = ['buildifier']
