" ftplugin/qf.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
setlocal nowrap
setlocal number
