" ftplugin/zig.vim
" Copyright 2024 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
packadd zig
let b:ale_fixers = ['zigfmt']
