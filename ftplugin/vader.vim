" ftplugin/vader.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
packadd vader
setlocal foldmethod=marker
setlocal commentstring=\"%s
