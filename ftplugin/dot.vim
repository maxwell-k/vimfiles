" ftplugin/dot.vim
" Copyright 2022 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
setlocal tabstop&
setlocal softtabstop&
setlocal shiftwidth&
setlocal expandtab&
