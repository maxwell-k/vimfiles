" ftplugin/netrw.vim
" Copyright 2020 Keith Maxwell
" SPDX-License-Identifier: MPL-2.0
"
let g:netrw_banner=0
