#!/bin/sh
# tests/automated/transform/run.sh
# Copyright 2021 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
vim "+source run.vim"
