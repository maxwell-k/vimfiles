#!/bin/sh
# tests/automated/cog/run.sh
# Copyright 2021 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
cp missing.txt example.txt \
&& vim \
  "+edit example.txt" \
  "+set filetype=text.cog" \
  '+normal \aq' \
&& git diff --exit-code example.txt
