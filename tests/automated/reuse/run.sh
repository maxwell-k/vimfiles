#!/bin/sh
# tests/automated/reuse/run.sh
# Copyright 2021 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
uv --offline tool run reuse lint
