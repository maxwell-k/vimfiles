#!/bin/sh
# tests/automated/gitmodules/run.sh
# Copyright 2025 Keith Maxwell
# SPDX-License-Identifier: MPL-2.0
#
gitmodules="$(git rev-parse --show-toplevel)/.gitmodules"
if [ ! -f "$gitmodules" ] ; then
  printf 'Failure: .gitmodules not found\n'
  exit 1
fi
if git grep --perl-regexp 'url = (?!https://)' "$gitmodules" ; then
  printf 'Failure: .gitmodules should only use HTTPS urls\n'
  exit 1
fi

